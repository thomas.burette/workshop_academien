<?php 
//Config Mysqli
session_start();
include('../../include/config.inc.php');

$prenom = "";
$nom= "";
$mail = "";
$civilite;
$pays = "";
$ville = "";
$tel = "";
$role;
$errors = array();



if (isset($_POST['create_btn'])){
    createUser();
}

function createUser() {
    // Permet d'appeller les variables utilisés dans la fonction
    global $lien, $errors , $prenom , $nom , $mail , $pays , $ville , $tel , $role , $civilite ;

    // Valeur recuperé du form en html avec la function e() décalré plus en bas. 
    $prenom     =  e($_POST['prenom']);
    $nom        =  e($_POST['nom']);
    $mail       =  e($_POST['mail']);
    $civilite   =  '1';
    $pays       =  e($_POST['pays']);
    $ville      =  e($_POST['ville']);
    $tel        =  e($_POST['tel']);
    $role        =  '1';
	$password_1  =  e($_POST['password_1']);
    $password_2  =  e($_POST['password_2']);

    // Valider que le form est bien remplie et sinon les errors iront dans $errors

    if (empty($prenom)) { 
		array_push($errors, "Prénom non retenu"); 
    }
    elseif (empty($nom)) { 
		array_push($errors, "Nom non retenu"); 
	}
	elseif (empty($mail)) { 
		array_push($errors, "Email est nécessaire"); 
	}
	elseif (empty($password_1)) { 
		array_push($errors, "Password est nécessaire"); 
	}
	elseif ($password_1 != $password_2) {
		array_push($errors, "Les deux passwords sont différentes");
    }
    
    // Enregistrer le nouveau admin dans la base de données s'il n'ya pas d'erreurs

    if(count($errors) == 0){
        $password = password_hash($password_1, PASSWORD_DEFAULT); //Crypter le password saisi
        
            $insert = "INSERT INTO user (user_mail, user_mdp, user_prenom, user_nom , user_tel , user_pays , user_ville , user_civilite, user_role)
            VALUES('$mail', '$password', '$prenom', '$nom', '$tel', '$pays', '$ville', '$civilite', '$role')";
            mysqli_query($lien, $insert);
            $_SESSION['success']= "User ajouté!!";
            header('location: ../inscription2.php');
            $_SESSION['prenom'] = $prenom;
    }
};

function e($val) {
    global $lien;
    return mysqli_real_escape_string($lien, trim($val));

    // mysqli_real_escape_string protège les caractères spéciaux d'une chaîne pour l'utiliser dans une requête SQL.
    // trim supprime les espaces (ou d'autres caractères) en début et fin de chaîne
}

function show_error() {
    global $errors;
    
	if (count($errors) > 0){
		echo '<div>';
			foreach ($errors as $error ){
				echo $error .'<br>';
			}
		echo '</div>';
    }
    
}


?>

