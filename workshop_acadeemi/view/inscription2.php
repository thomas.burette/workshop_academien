<?php $title_page = "Inscription";
include './header.php'; ?>
<?php include '../include/config.inc.php'; ?>
<?php include '../include/form.php'; ?>


<section class="sectionInscription--header">
	<h2 class="sectionInscription--title">Inscription</h2>
</section>
<form class="sectionLog" method="post" action="./user/role.php">
    <div class="sectionLog__container">
        <div class="sectionLog__container-top top1">
            <p>30%</p>
        </div>
        <div class="sectionLog__container-bottom">
            <div class="sectionLog__container-bottom-text">
                <p>Cours d'introduction : dev , market , design</p>
                <p>Resumé du cours</p>
                <p>Quizz</p>
            </div>
            <div>
            <?php echo $_SESSION['prenom']; ?>
                <form action="./user/role" method="post">
                <button type="submit" class="sectionLog__container-bottom-button1" name="role" value="1">Confirmer</button>
                </form>
            </div>
        </div>
    </div>
    <div class="sectionLog__container">
        <div class="sectionLog__container-top top2">
            <p>100%</p>
        </div>
        <div class="sectionLog__container-bottom">
            <div class="sectionLog__container-bottom-text">
                <p>Plus de chapitre - plus détaillé</p>
                <p>Jeu et exercice</p>
                <p>Vidéo et tuto</p>
            </div>
            <div class="sectionLog__container-bottom-button2">
                <p>150€/mois</p>
                <form action="./user/role" method="post">
                <button type="submit" class="sectionLog__container-bottom-button1" name="role" value="2">Confirmer</button>
                </form>
            </div>
        </div>
    </div>
    <div class="sectionLog__container">
        <div class="sectionLog__container-top top3">
            <p>100% - EEMI</p>
        </div>
        <div class="sectionLog__container-bottom">
            <div class="sectionLog__container-bottom-text">
                <p>Cours entier - Tous les chapitres - Exercice</p>
                <p>Explication</p>
                <p>Poser des questions via forum</p>
            </div>
            <div class="sectionLog__container-bottom">
                <div >
                <form action="./user/role.php" method="post">
                <button type="submit" class="sectionLog__container-bottom-button1" name="role" value="2">Confirmer</button>
                </form>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript" src="../scripts/index.js"></script>
</body>

</html>