#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: civilite
#------------------------------------------------------------

CREATE TABLE civilite(
        id_civilite  Int  Auto_increment  NOT NULL ,
        civilite_nom Varchar (50) NOT NULL
	,PRIMARY KEY (id_civilite)
)ENGINE=MyISAM;


#------------------------------------------------------------
# Table: role
#------------------------------------------------------------

CREATE TABLE role(
        id_role  Int  Auto_increment  NOT NULL ,
        role_nom Varchar (100) NOT NULL
	,PRIMARY KEY (id_role)
)ENGINE=MyISAM;


#------------------------------------------------------------
# Table: user
#------------------------------------------------------------

CREATE TABLE user(
        id_user     Int  Auto_increment  NOT NULL ,
        user_mail   Varchar (100) NOT NULL ,
        user_mdp    Varchar (100) NOT NULL ,
        user_prenom Varchar (100) NOT NULL ,
        user_nom    Varchar (100) NOT NULL ,
        user_tel    Varchar (10) NOT NULL ,
        user_pays   Varchar (50) NOT NULL ,
        user_ville  Varchar (50) NOT NULL ,
        user_civilite Int NOT NULL ,
        user_role     Int NOT NULL
	,PRIMARY KEY (id_user)
)ENGINE=MyISAM;


#------------------------------------------------------------
# Table: categorie
#------------------------------------------------------------

CREATE TABLE categorie(
        id_categorie  Int  Auto_increment  NOT NULL ,
        categorie_nom Varchar (200) NOT NULL
	, PRIMARY KEY (id_categorie)
)ENGINE=MyISAM;


#------------------------------------------------------------
# Table: article
#------------------------------------------------------------

CREATE TABLE article(
        id_article     Int  Auto_increment  NOT NULL ,
        article_titre  Varchar (200) NOT NULL ,
        article_date   Date NOT NULL ,
        article_auteur Varchar (200) NOT NULL ,
        article_categorie   Int NOT NULL
	,PRIMARY KEY (id_article)
)ENGINE=MyISAM;


#------------------------------------------------------------
# Table: section
#------------------------------------------------------------

CREATE TABLE section(
        id_section      Int  Auto_increment  NOT NULL ,
        section_contenu Text NOT NULL
	,PRIMARY KEY (id_section)
)ENGINE=MyISAM;


#------------------------------------------------------------
# Table: quizz
#------------------------------------------------------------

CREATE TABLE quizz(
        id_quizz   Int  Auto_increment  NOT NULL ,
        quizz_section Int NOT NULL
	,PRIMARY KEY (id_quizz)
)ENGINE=MyISAM;


#------------------------------------------------------------
# Table: question
#------------------------------------------------------------

CREATE TABLE question(
        id_question             Int  Auto_increment  NOT NULL ,
        question_contenu        Varchar (200) NOT NULL ,
        question_mauvaise_rep_1 Text NOT NULL ,
        question_mauvaise_rep_2 Text NOT NULL ,
        question_mauvaise_rep_3 Text NOT NULL ,
        question_bonne_rep      Text NOT NULL ,
        question_quizz                Int NOT NULL
	,PRIMARY KEY (id_question)
)ENGINE=MyISAM;


#------------------------------------------------------------
# Table: possede
#------------------------------------------------------------

CREATE TABLE possede(
        id_possede Int Auto_increment NOT NULL,
        possede_section Int NOT NULL ,
        possede_article Int NOT NULL
	,PRIMARY KEY (id_possede)
)ENGINE=MyISAM;

