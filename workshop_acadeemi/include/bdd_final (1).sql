-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 17 avr. 2019 à 16:36
-- Version du serveur :  5.7.21
-- Version de PHP :  5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `acadeemi`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `id_article` int(11) NOT NULL AUTO_INCREMENT,
  `article_titre` varchar(200) NOT NULL,
  `article_date` date NOT NULL,
  `article_auteur` varchar(200) NOT NULL,
  `article_categorie` int(11) NOT NULL,
  PRIMARY KEY (`id_article`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`id_article`, `article_titre`, `article_date`, `article_auteur`, `article_categorie`) VALUES
(1, 'Les réseaux sociaux', '2019-04-15', 'Emma Benhamou', 3),
(2, 'Les réseaux sociaux', '2019-04-15', 'Emma Benhamou', 3);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id_categorie` int(11) NOT NULL AUTO_INCREMENT,
  `categorie_nom` varchar(200) NOT NULL,
  PRIMARY KEY (`id_categorie`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id_categorie`, `categorie_nom`) VALUES
(1, 'Web developpement '),
(2, 'Interactive Design'),
(3, 'E-business');

-- --------------------------------------------------------

--
-- Structure de la table `civilite`
--

DROP TABLE IF EXISTS `civilite`;
CREATE TABLE IF NOT EXISTS `civilite` (
  `id_civilite` int(11) NOT NULL AUTO_INCREMENT,
  `civilite_nom` varchar(50) NOT NULL,
  PRIMARY KEY (`id_civilite`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `civilite`
--

INSERT INTO `civilite` (`id_civilite`, `civilite_nom`) VALUES
(1, 'Madame'),
(2, 'Monsieur'),
(3, 'Autre');

-- --------------------------------------------------------

--
-- Structure de la table `possede`
--

DROP TABLE IF EXISTS `possede`;
CREATE TABLE IF NOT EXISTS `possede` (
  `id_possede` int(11) NOT NULL AUTO_INCREMENT,
  `possede_section` int(11) NOT NULL,
  `possede_article` int(11) NOT NULL,
  PRIMARY KEY (`id_possede`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `question`
--

DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `id_question` int(11) NOT NULL AUTO_INCREMENT,
  `question_contenu` varchar(200) NOT NULL,
  `question_mauvaise_rep_1` text NOT NULL,
  `question_mauvaise_rep_2` text NOT NULL,
  `question_mauvaise_rep_3` text NOT NULL,
  `question_bonne_rep` text NOT NULL,
  `question_quizz` int(11) NOT NULL,
  PRIMARY KEY (`id_question`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `quizz`
--

DROP TABLE IF EXISTS `quizz`;
CREATE TABLE IF NOT EXISTS `quizz` (
  `id_quizz` int(11) NOT NULL AUTO_INCREMENT,
  `quizz_section` int(11) NOT NULL,
  PRIMARY KEY (`id_quizz`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `role_nom` varchar(100) NOT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`id_role`, `role_nom`) VALUES
(1, 'visiteur'),
(2, 'etudiant');

-- --------------------------------------------------------

--
-- Structure de la table `section`
--

DROP TABLE IF EXISTS `section`;
CREATE TABLE IF NOT EXISTS `section` (
  `id_section` int(11) NOT NULL AUTO_INCREMENT,
  `section_contenu` text NOT NULL,
  `section_article` int(11) NOT NULL,
  PRIMARY KEY (`id_section`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `section`
--

INSERT INTO `section` (`id_section`, `section_contenu`, `section_article`) VALUES
(1, '	<p class=\"sectionArticle__paragraphe\">Les réseaux sociaux sont une notion ambigüe car au sens strict, un réseau social désigne les différentes relations que les individus entretiennent entre eux et la façon dont celles-ci se structurent; ces différentes relations permettent de comprendre les comportements des individus. </p>\r\n	<p class=\"sectionArticle__paragraphe\">Dans ce cours, vous aurez l’occasion d’en apprendre plus sur l’histoire des réseaux sociaux, les différents réseaux sociaux, leurs usages, les intentions des utilisateur et les différents utilisateurs sur les réseaux sociaux.</p>\r\n	<h4 class=\"sectionArticle__sommaire--title\">Sommaire du cours complet :</h4>\r\n\r\n	<h3 class=\"sectionArticle__sommaire--name\"><span class=\"sectionArticle__sommaire--number\">1. </span>Définition des réseaux sociaux</h3>\r\n	<p class=\"sectionArticle__sommaire-point\">1.1 Définition</p>\r\n	<p class=\"sectionArticle__sommaire-point\">1.2 Histoire</p>\r\n\r\n	<h3 class=\"sectionArticle__sommaire--name\"><span class=\"sectionArticle__sommaire--number\">2. </span>Les réseau sociaux</h3>\r\n	<p class=\"sectionArticle__sommaire-point\">2.1 Facebook</p>\r\n	<p class=\"sectionArticle__sommaire-point\">2.2 Pinterest</p>\r\n	<p class=\"sectionArticle__sommaire-point\">2.3 Tumblr</p>\r\n	<p class=\"sectionArticle__sommaire-point\">2.4 YouTube</p>\r\n	<p class=\"sectionArticle__sommaire-point\">2.5 LinkedIn</p>\r\n	<p class=\"sectionArticle__sommaire-point\">2.6 Snapchat</p>\r\n	<p class=\"sectionArticle__sommaire-point\">2.7 Instagram</p>\r\n	<p class=\"sectionArticle__sommaire-point\">2.8 Twitter</p>\r\n\r\n	<h3 class=\"sectionArticle__sommaire--name\"><span class=\"sectionArticle__sommaire--number\">3. </span>Intentions des utilisateurs sur les réseaux sociaux </h3>\r\n\r\n	<h3 class=\"sectionArticle__sommaire--name\"><span class=\"sectionArticle__sommaire--number\">4. </span>Les différents utilisateurs sur les réseaux sociaux</h3>\r\n\r\n	<h3 class=\"sectionArticle__sommaire--name\"><span class=\"sectionArticle__sommaire--number\">5. </span>Les différentes mutations sur les réseaux sociaux</h3>\r\n\r\n	<img class=\"sectionArticle__img\" src=\"../assets/images/socialmedia.jpg\" alt=\"une banderole sur les réseaux sociaux\">\r\n	<p class=\"sectionArticle__paragraphe\">Les réseaux sociaux font parti de notre monde, il faut savoir bien les utiliser. Chaque réseau social a un but bien précis, tout comme chaque utilisateur a un objectif en utilisant tel ou tel réseau social.</p>\r\n	<p class=\"sectionArticle__paragraphe\">Par exemple, Linkedin est un réseau social plus spécialisé, qui est destiné aux entreprises et aux professionnels.</p>\r\n	<p class=\"sectionArticle__paragraphe\">Chaque utilisateur a la possibilité de créer un profil où il peut librement afficher son expérience professionnelle et ses compétences. Le réseau social met ainsi en contact des millions de professionnels, dans le contexte professionnelle on peut gagner du poids et c’est facile de se créer une notoriété.</p>\r\n	<p class=\"sectionArticle__paragraphe\">Les réseaux sociaux ont connus une croissance très importante au cours des dernières années. En effet, en étant seulement à ses débuts les réseaux sociaux ont déjà provoqué des milliers de changements à travers le monde. Ils ont révolutionnés la façon de communiquer et comment les gens interagissent ensemble.</p>\r\n	<p class=\"sectionArticle__paragraphe\">Les réseaux sociaux ne cessent d’évoluer au fil du temps, de se modifier, d’ajouter des fonctionnalités et deviennent aujourd’hui indispensables.</p>\r\n\r\n	<div class=\"sectionArticle__quizz\">\r\n		<h3 class=\"sectionArticle__quizz--title\">Teste tes compétences avec ce quizz en réseaux sociaux</h3>', 1),
(2, '<h3 class=\"sectionArticle__bigTitle\">1. Définition des réseaux sociaux</h3>\r\n<h5 class=\"sectionArticle__littleTitle\">1.1 Définition</h5>\r\n<p class=\"sectionArticle__paragraphe\">En sciences humaines et sociales, l\'expression réseau social désigne un agencement de liens entre des individus et/ou des organisations, constituant un groupement qui a un sens : la famille, les collègues, un groupe d\'amis, une communauté, etc.\r\nL’expression « réseau social » dans l\'usage habituel renvoie généralement à celle de « médias sociaux », qui recouvre les différentes activités qui intègrent technologie, interaction sociale entre individus ou groupes d\'individus, et la création de contenu.</p>\r\n<h5 class=\"sectionArticle__littleTitle\">1.2 Histoire</h5>\r\n<p class=\"sectionArticle__paragraphe\">La première graine de ce qui peut être considéré comme un réseau social a été en 1971, avec le premier e-mail qui a été échangé entre deux ordinateurs qui étaient côte à côte.</p>\r\n<p class=\"sectionArticle__paragraphe\">Plus tard, en 1978, un BBS (Bulleting Board System) a été échangé avec d’autres utilisateurs via des lignes téléphoniques. \r\nDes années plus tard, en 1994, GeoCities, qu’on pourrait considérer comme l’un des premiers réseaux sociaux, a été lancé.</p>\r\n<p class=\"sectionArticle__paragraphe\">Un an plus tard, en 1995, TheGlobe.com voit le jour. Ce réseau social permettait à ses utilisateurs de personnaliser leur expérience en ligne en publiant leur propre contenu et en interagissant avec d’autres personnes ayant des intérêts similaires.</p>\r\n<p class=\"sectionArticle__paragraphe\">La même année, Classmates est né : un site web qui aidait les gens à retrouver leurs anciens camarades de classe et collègues.</p>\r\n<p class=\"sectionArticle__paragraphe\">Deux ans plus tard, c’était au tour de SixDegrees. C’était un espace virtuel que certains considèrent comme le premier réseau social, ou du moins celui qui correspond le mieux à la définition d’un réseau social.</p>\r\n<p class=\"sectionArticle__paragraphe\">En 1997, Instant Messenger a été lancé, c’était un programme de messagerie instantanée créé par Microsoft Windows permettant aux utilisateurs d’utiliser des services élémentaires de chat et des listes de contacts. On peut le voir comme le précurseur de l’un des réseaux sociaux les plus puissants actuellement, le service de messagerie instantanée WhatsApp.</p>\r\n<p class=\"sectionArticle__paragraphe\">L’avènement de réseaux sociaux tels que LinkedIn ou Flickr et d’espaces comme Digg, WordPress ou YouTube laissait présager un séisme jusqu’alors inédit sur Internet. C’est ainsi qu’en 2004, l’un des plus grands réseaux sociaux a été créé : Facebook.</p>\r\n<p class=\"sectionArticle__paragraphe\">Ce nouveau réseau social n’a pas tardé à dépasser MySpace en tant que leader en nombre de visiteurs mensuels. Son succès a été tel que MySpace a dû modifier sa stratégie.\r\nEn 2006, Facebook s’est ouvert au public. Cela a bouleversé l’histoire des réseaux sociaux.</p>\r\n<p class=\"sectionArticle__paragraphe\">Ce réseau social est reconnu comme l’un des plus importants de tous les temps. C’est celui qui compte le plus grand nombre d’utilisateurs : 1,650 milliard d’utilisateurs actifs mensuels au 27 avril 2016. Et ce n’est pas prêt de s’arrêter.</p>\r\n<p class=\"sectionArticle__paragraphe\">Deux ans plus tard, en 2006, naissait Twitter, un outil conçu comme un système de messagerie interne pour donner des nouvelles, mais qui a changé la façon de regarder la télévision, voire de vivre les révolutions.</p>\r\n<p class=\"sectionArticle__paragraphe\">Ces dernières années, de plus en plus de réseaux sociaux ont vu le jour et se sont popularisés dans des publics de niche possédant des caractéristiques propres. Parmi les réseaux sociaux des cinq dernières années. </p>\r\n<h3 class=\"sectionArticle__bigTitle\">2. Les réseaux sociaux</h3>\r\n<h5 class=\"sectionArticle__littleTitle\">2.1 Facebook</h5>\r\n<p class=\"sectionArticle__paragraphe\">c’est une plateforme de partage de contenu entre utilisateurs, ça permet aux utilisateurs de publier des textes, des vidéos,des images ou des liens. C’est aussi considéré comme une vitrine de marque. Sur cette plateforme, on publie, on diffuse mais on reçoit très peu de contenu. C’est un média où on peut publier des publicité, cependant il est quasiment impossible d’avoir une présence organique.</p>\r\n<h5 class=\"sectionArticle__littleTitle\">2.2 Pinterest</h5>\r\n<p class=\"sectionArticle__paragraphe\">C’est une plateforme de partage d’images entre utilisateurs. Elle s’organise autour de tableaux personnels organisés par thème, où les utilisateurs conservent des images selon différents événements ou passe-temps.</p>\r\n<h5 class=\"sectionArticle__littleTitle\">2.3 Tumblr</h5>\r\n<p class=\"sectionArticle__paragraphe\">C’est une plateforme de microblogging qui permet aux utilisateurs de publier des textes, des vidéos, des images, des liens ou des citations….</p>\r\n<a class=\"sectionArticle__quizz--link\" href=\"./market_reseaux-sociaux_chap3.php\">Cours suivant</a>\r\n</section>', 1),
(3, '<h3 class=\"sectionArticle__bigTitle\">2. Les réseaux sociaux</h3>\r\n<h5 class=\"sectionArticle__littleTitle\">2.4 YouTube</h5>\r\n<p class=\"sectionArticle__paragraphe\">Un site web où les utilisateurs peuvent télécharger et partager du contenu vidéo. Ce site héberge des millions de clips musicaux, de programmes télévisés et de vidéoblogs.</p>\r\n<h5 class=\"sectionArticle__littleTitle\">2.5 LinkedIn</h5>\r\n<p class=\"sectionArticle__paragraphe\">C’est un réseau social plus spécialisé, qui est destiné aux entreprises et aux professionnels. Chaque utilisateur a la possibilité de créer un profil où il peut librement afficher son expérience professionnelle et ses compétences. Le réseau social met ainsi en contact des millions de professionnels, dans le contexte professionnelle on peut gagner du poids et c’est facile de se créer une notoriété.</p>\r\n<h5 class=\"sectionArticle__littleTitle\">2.6 Snapchat</h5>\r\n<p class=\"sectionArticle__paragraphe\">Cette application mobile vous permet d’envoyer des fichiers sous forme d’images ou de vidéos qui disparaissent du mobile du destinataire entre une et dix secondes après réception. Tout le contenu est envoyé sous forme de messages privés.</p>\r\n<h5 class=\"sectionArticle__littleTitle\">2.7 Instagrame</h5>\r\n<p class=\"sectionArticle__paragraphe\">Cette plateforme permet de publier des photos ou des vidéos, c’est souvent du contenu de qualité. Cette application est tout dans le visuel, pour les entreprises ça permet d’avoir une belle vitrine cependant il y a très peu d’échanges. Ce réseau social permet de réunir des gens du monde entier en publier des photos de leurs vie ou de voyage mais cependant on a toujours la possibilité de se mettre en privée.</p>\r\n<h5 class=\"sectionArticle__littleTitle\">2.8 Twitter</h5>\r\n<p class=\"sectionArticle__paragraphe\">Cette plateforme permet aux utilisateurs de publier des textes, des citations, rarement des photos et vidéos. Cependant le nombre de caractères est limité à 140 mais depuis peu c’est 280, ça permet donc un échange plus sympa et plus long. On ne peut pas mettre n’importe quoi sur Twitter car  il y a beaucoup de militant sur tous les sujets. Le but de cette plateforme est l’échange et le partage. Pour les entreprises, c’est un excellent support de retours clients. </p>\r\n<h3 class=\"sectionArticle__bigTitle\">3. Les intentions des utilisateurs sur les réseaux sociaux</h3>\r\n<p class=\"sectionArticle__paragraphe\"><span class=\"sectionArticle__littleTitle\">Facebook : </span>les utilisateurs de facebook, vont sur ce réseau pour publier des images, des vidéos, des fichiers, des documents et d’échanger des messages, rejoindre et créer des groupes et d\'utiliser une variété d\'application, événement, garder en contact avec les gens, s’informer, apprendre, augmenter la visibilité de son entreprise, s’inspirer, publier du contenu, créer des groupes, créer des pages</p>\r\n<p class=\"sectionArticle__paragraphe\"><span class=\"sectionArticle__littleTitle\">YouTube : </span>les utilisateurs de Youtube, vont sur ce réseau pour regarder, publier des vidéos, écouter de la musique, regarder des films, s’informer et apprendre, donner son avis et partager ses expériences.</p>\r\n<p class=\"sectionArticle__paragraphe\"><span class=\"sectionArticle__littleTitle\">LinkedIn : </span> les utilisateurs de Linkedin vont sur ce réseaux pour gagner du poids et une visibilité au niveau du professionnel. Mais ça sert aussi à trouver un job, recruter des personnes, avoir des relations professionnelles, partage de contenus, on peut poser des questions à des experts, mais aussi participer et organiser des événements</p>\r\n<p class=\"sectionArticle__paragraphe\"><span class=\"sectionArticle__littleTitle\">Instagram : </span>les utilisateurs d’instagram, vont sur ce réseaux pour publier des photos ou vidéos en direct ou en vrai post avec leur communautés. Ça peut permettre de faire de la pub pour une marque de vêtement, de bijoux, des restaurants, de boite de nuit, des entreprises… Mais aussi permettre de montrer qui on est et de montrer une autre facette de nous et  d’attirer de nouveaux contacts.  Ça renforce un impact visuel,  car sur ce réseau on voit seulement des images donc montrer notre fibre artistique  donc notre créativité. </p>\r\n<h3 class=\"sectionArticle__bigTitle\">4. Les différents utilisateurs sur les réseaux sociaux</h3>\r\n<p class=\"sectionArticle__paragraphe\"><span class=\"sectionArticle__littleTitle\">Facebook : </span>une cible plutôt jeune avec un cœur de cible se plaçant sur les 18 – 29 ans, soit 83% de cette tranche d’âge contre 53% pour la tranche 50-64 ans. Facebook apparaît comme étant la plateforme la plus populaire et la plus jeune. La présence des marques globales de la grande distribution et de la consommation paraît alors cohérente avec la cible.</p>\r\n<p class=\"sectionArticle__paragraphe\"><span class=\"sectionArticle__littleTitle\">Twitter : </span>le rassembleur,même si la tranche des 18-29 ans est légèrement majoritaire, Twitter est la plateforme la plus fédératrice en termes de profils différents. Tous les revenus, toutes les activités, y sont bien représentés. Investir dans  Twitter offre l’avantage de toucher une cible multiple, qui correspond à de nombreuses démarches marketing différentes.\r\n</p>\r\n<p class=\"sectionArticle__paragraphe\"><span class=\"sectionArticle__littleTitle\">LinkedIn : </span>des profils plus âgés aux revenus importants. LinkedIn concentre majoritairement la tranche des 30-49 ans, gagnant plus de 75 000 dollars annuel. Il s’agit pour la plupart de cadres ou dirigeants d’entreprise. Une approche B2B semble être bien pertinente avec le profil des inscrits.\r\n</p>\r\n<p class=\"sectionArticle__paragraphe\"><span class=\"sectionArticle__littleTitle\">Pinterest & Instagram : </span> les deux « nouveaux ». Sur ces deux plateformes relativement récentes, les tendances sont : Une place prépondérante de femmes pour Pinterest : 20% des inscrits - La tranche des 18-29 ans est majoritaire chez Instagram : pour près de 30%.\r\n</p>\r\n<a class=\"sectionArticle__quizz--link\" href=\"./market_reseaux-sociaux_chap4.php\">Cours suivant</a>\r\n</section>', 1),
(4, '<h3 class=\"sectionArticle__bigTitle\">5. Les différentes mutations des réseaux\r\nsociaux</h3>\r\n<p class=\"sectionArticle__paragraphe\">Les réseaux sociaux ont connus une croissance très importante au cours des dernières années. En effet, en étant seulement à ses débuts les réseaux sociaux ont déjà provoqué des milliers de changements à travers le monde. Ils ont révolutionnés la façon de communiquer et comment les gens interagissent ensemble.</p>\r\n<p class=\"sectionArticle__paragraphe\">Les réseaux sociaux ne cessent d’évoluer au fil du temps, de se modifier, d’ajouter des fonctionnalités et deviennent aujourd’hui indispensables. </p>\r\n<p class=\"sectionArticle__paragraphe\">En effet, aujourd’hui les réseaux sociaux sont devenus indispensables tant à des fins personnelles que professionnelles.\r\nOn remarque au cours du temps que l’utilisation des réseaux sociaux n’a cessé d’augmenter et que l’on prévoit que cette expansion continue jusqu’au moins 2020.</p>\r\n<p class=\"sectionArticle__paragraphe\">Les évolutions constatées depuis la création des principaux réseaux sociaux : </p>\r\n<h5 class=\"sectionArticle__littleTitle\">Facebook : </h5>\r\n<p class=\"sectionArticle__paragraphe\">Le logo a évolué<br>\r\nEvolution de l’algorithme d’affichage des publications<br> \r\nMise en place des call to action (2015) -> se rediriger vers d’autres applications ou site web<br>\r\nUtilisation du live<br>\r\nPartage par message privé<br>\r\nApparition des story<br>\r\nCréation de l’application messenger pour gérer la messagerie <br>instantanée de Facebook<br>\r\n</p>\r\n<h5 class=\"sectionArticle__littleTitle\">Twitter</h5>\r\n<p class=\"sectionArticle__paragraphe\">Vidéos<br>\r\nDéveloppement des messages privés<br>\r\nAnnonces filtrées et ciblées<br>\r\nMessage direct plus limités en terme de caractères<br>\r\nRecherche avec un lieux précis<br>\r\nTweetdeck -> permet de mieux s’organiser sur twitter (listes)<br>\r\n</p>\r\n<h5 class=\"sectionArticle__littleTitle\">Instagram</h5>\r\n<p class=\"sectionArticle__paragraphe\">Instagram a été racheté par Facebook en 2012<br>\r\nChangement du logo<br>\r\nLancement de Layout et Boomerang (applications crées par instagram pour montage et boomerang)<br>\r\nPublicité avec des posts sponsorisés<br>\r\nPhoto en format paysage<br>\r\nLancement d’une messagerie instantanée<br>\r\nLancement des story suite au succès de celles-ci sur instagram<br>\r\nRécemment apparition des live<br>\r\nRécemment apparition des audios en dm<br>\r\n</p>\r\n<h5 class=\"sectionArticle__littleTitle\">Snapchat</h5>\r\n<p class=\"sectionArticle__paragraphe\">Apparition de la fonctionnalité message<br>\r\nApparitions des publicités (spots qui se vendent extrêmement chers jusqu’à 750 000$)<br>\r\nLancement de Snapcash qui permet le transfert d’argent des utilisateurs<br>\r\nLancement de Discover (récapitulatif de l’actualité du jour à travers <br>10 médias américains)<br>\r\nPassage de la fonction Replay de payante (0.99€) à gratuite<br>\r\nApparition des filtres<br> \r\n</p>\r\n<h5 class=\"sectionArticle__littleTitle\">Whatsapp</h5>\r\n<p class=\"sectionArticle__paragraphe\">\r\nRachat par facebook<br>\r\nApparition des story<br>\r\nFonctionnalité appels vidéos<br>\r\nGroupes avec plus de monde<br>\r\naudios<br>\r\nOn assiste à un développement des réseaux sociaux en fonctions des autres. En effet, par exemple les story ont été créés sur de nombreux réseaux sociaux suite au succès de celles-ci sur snapchat.\r\n</p>\r\n<p class=\"sectionArticle__paragraphe\">Aujourd’hui (2019) les réseaux sociaux les plus utilisés sont : </p>\r\n<p class=\"sectionArticle__paragraphe\">Facebook <br>\r\nYoutube<br>\r\nInstagram<br>\r\nWhatsapp<br>\r\nMessenger<br>\r\n</p>\r\n<p class=\"sectionArticle__paragraphe\">On note une remontée d’instagram notamment avec la mise en place des story qui ont été un vrai plus pour l’application face à l’expansion de Snapchat.</p>\r\n<p class=\"sectionArticle__paragraphe\">Apparition avec l’évolution des réseaux sociaux de plateformes comme Buffer permettant de planifier des posts sur Facebook, Twitter, Google +, Pinterest ou encore Linkedin. De plus, ce type de plateforme peut te permettre une automatisation de publication d’article.</p>\r\n', 1);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `user_mail` varchar(100) NOT NULL,
  `user_mdp` varchar(100) NOT NULL,
  `user_prenom` varchar(100) NOT NULL,
  `user_nom` varchar(100) NOT NULL,
  `user_tel` varchar(10) NOT NULL,
  `user_pays` varchar(50) NOT NULL,
  `user_ville` varchar(50) NOT NULL,
  `user_civilite` int(11) NOT NULL,
  `user_role` int(11) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id_user`, `user_mail`, `user_mdp`, `user_prenom`, `user_nom`, `user_tel`, `user_pays`, `user_ville`, `user_civilite`, `user_role`) VALUES
(1, 'jean.dupont@gmail.com', '123', 'Jean', 'Dupont', '0646874534', 'France', 'Paris', 1, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
