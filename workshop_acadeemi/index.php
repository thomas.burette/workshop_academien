<?php $title_page = 'Acad\'EEMI'; ?>
<!DOCTYPE html>
<html lang="en">
	<? session_start()?>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="./styles/style.css" type="text/css" />
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<title><?php echo $title_page; ?></title>
</head>
<body>
	<header>
	<div class="texte_header"> 
	<a href="#">Le Forum</a>
	</div>
	<div class="texte_header2"> 
	<a href="./view/cours.php">Les Cours</a>
	</div>
	<div class="texte_header3"> 
	<a href="./view/inscription.php">S'inscrire</a>
	</div>
	<div class="texte_header4"> 
	<a href="./view/connexion.php">Se Connecter</a>
	</div>
	<div id="hidden" class="sectionMenu">
		<div>
			<p onclick="menu('hide')" class="sectionMenu__back"><- Retour</p>
			<div id="menu" class="sectionMenu__container">
				<p onclick="connect()" class="sectionMenu__link">Se connecter</p>
				<a class="sectionMenu__link" href="./view/inscription.php">S'inscrire</a>
				<a class="sectionMenu__link" href="./index.php">Acceuil</a>
				<a class="sectionMenu__link" href="./view/cours.php">Les cours</a>
			</div>
			<div id="connect" class="sectionMenu__connect">
				<? echo('Welcom'.$_SESSION['prenom']) ?>
				<p class="sectionMenu__title">Se connecter</p>
				<form action='./view/user/user.php' method='type'>
				<input class="sectionMenu__input" type="text" name="mail" placeholder="E-mail">
				<input class="sectionMenu__input" type="password" name="pass" placeholder="Mot de passe">
				<button class="sectionMenu__button" type="submit">Connexion</button>
				</form>
				<div class="sectionMenu__google--container">
					<img  class="sectionMenu__google--logo" src="../assets/images/logo-google.png" alt="Logo de Google">
					<p class="sectionMenu__google--texte">Avec Google</p>
				</div>
			</div>
		</div>
	</div>
	<header class="sectionHeader">
		<img class="sectionHeader__img" src="./assets/images/logo-acadeemi.png" alt="Une photo de notre logo - ACAD'EEMI">
		<div onclick="menu('show')" class="sectionHeader__menu--container">
			<div class="sectionHeader__menu--item"></div>
			<div class="sectionHeader__menu--item"></div>
			<div class="sectionHeader__menu--item"></div>
		</div>
	</header>
<div class="sectionInscription">
	<h2 class="sectionInscription__title">Acad'eemi</h2>
	<a href=".//view/inscription.php"><button class="sectionInscription__button">Je m'inscris !</button></a>
</div>
<div class="sectionConcept">
	<img src="./assets/images/palais-img.jpg" class="sectionConcept__img" alt="Une photo du palais Brongniart">
	<div class="sectionConcept__texte">
		<h4 class="sectionConcept__title">Notre Concept</h4>
		<p class="sectionConcept__paragraphe">Nous proposons une plateforme de service de formation en ligne, basé sur les cours de l'EEMI. Sur cette plateforme, des cours d'E-business, de Web Developpement ainsi que d'Interactive Design te seront proposés !</p>
	</div>
</div>
<div class="sectionConcept">
	<img src="./assets/images/eemi-img.jpg" class="sectionConcept__img" alt="Une photo de nos locaux">
	<div class="sectionConcept__texte">
		<h4 class="sectionConcept__title">Notre école</h4>
		<p class="sectionConcept__paragraphe">Free, Meetic et Vente-privee.com. Les fondateurs de ces trois entreprises, leaders du Net en Europe, ont décidé de créer ensemble l’EEMI pour répondre à leurs besoins propres ainsi qu’à ceux d’une profession porteuse d’avenir et en constante (r)évolution.</p>
		<p>En savoir plus</p>
	</div>
</div>
<?php
echo '<div class="containerweb">';
for ($i=0; $i<3;$i++){
	echo('
	<div class="webcontent">
	<img class="img"src="./assets/images/bot.png"/>
	<h4>Concept</h4>
	<p class="bottom">Plateforme de formation en ligne</p>
	
	</div>

	');
}

echo '</div>';
?>
<script type="text/javascript" src="./scripts/index.js"></script>
</body>
</html>
